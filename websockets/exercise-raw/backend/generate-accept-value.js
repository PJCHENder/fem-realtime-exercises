import crypto from "crypto";

/**
 * client 和 server 要建立 websocket 連線前，server 必須回傳對應的 "Sec-WebSocket-Accept"，
 * 表示它們雙方都可以進行 websocket 連線。
 * Server 要產生 "Sec-WebSocket-Accept" 的方式，需要先把 client 在 header 中的 "Sec-WebSocket-Key" 取出，
 * 然後將這個 key 和 "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"（magic string）接在一起後，
 * 進行 SHA1 的雜湊並以 base64 回傳，即可得到 "Sec-WebSocket-Accept"。
 * https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers
 **/
function generateAcceptValue(acceptKey) {
  return (
    crypto
      .createHash("sha1")
      // this magic string key is actually in the spec
      .update(acceptKey + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11", "binary")
      .digest("base64")
  );
}

export default generateAcceptValue;
