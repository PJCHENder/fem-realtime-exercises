import http from "http";
import handler from "serve-handler";
import nanobuffer from "nanobuffer";

// these are helpers to help you deal with the binary data that websockets use
import objToResponse from "./obj-to-response.js";
import generateAcceptValue from "./generate-accept-value.js";
import parseMessage from "./parse-message.js";

let connections = [];
const msg = new nanobuffer(50);
const getMsgs = () => Array.from(msg).reverse();

msg.push({
  user: "brian",
  text: "hi",
  time: Date.now(),
});

// serve static assets
const server = http.createServer((request, response) => {
  return handler(request, response, {
    public: "./frontend",
  });
});

// 當 server 收到要 upgrade 的請求
server.on("upgrade", (req, socket) => {

  // 如果不是 websocket 請求，則回覆 bad request
  if (req.headers["upgrade"] !== "websocket") {
    socket.end("HTTP/1.1 400 Bad Request");
    return;
  }

  const acceptKey = req.headers['sec-websocket-key'];
  const acceptValue = generateAcceptValue(acceptKey);
  const headers = [
    "HTTP/1.1 101 Switching Protocols",
    "Upgrade: WebSocket",
    "Connection: Upgrade",
    `Sec-WebSocket-Accept: ${acceptValue}`,
    `Sec-WebSocket-Protocol: json`,
    "\r\n"    // indicate that here is the end of header
  ];

  socket.write(headers.join("\r\n"));

  // When the client connects to serer at the first time, send back all msgs back to the client
  socket.write(objToResponse({ msg: getMsgs() }));

  // save the "socket" of the user into the connects for future use
  connections.push(socket);

  // received the message from the client
  socket.on("data", (buffer) => {
    const message = parseMessage(buffer);

    // save the message in the memory
    if (message) {
      msg.push({
        user: message.user,
        text: message.text,
        time: Date.now(),
      })

      // broadcast the message to all clients
      connections.forEach((s) => {
        s.write(objToResponse({ msg: getMsgs() }));
      })
    } else if (message === null) {
      // the message will be null if the client is closing the connection
      // so we close the connection with the client
      socket.end()
    }
  })

  // handle user disconnection
  socket.on("end", () => {
    connections = connections.filter((s) => s !== socket);
  })
})

const port = process.env.PORT || 8080;
server.listen(port, () =>
  console.log(`Server running at http://localhost:${port}`)
);
