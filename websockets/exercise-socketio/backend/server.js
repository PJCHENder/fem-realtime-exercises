import http from "http";
import handler from "serve-handler";
import nanobuffer from "nanobuffer";
import { Server } from "socket.io";

const msg = new nanobuffer(50);
const getMsgs = () => Array.from(msg).reverse();

msg.push({
  user: "brian",
  text: "hi",
  time: Date.now(),
});

// serve static assets
const server = http.createServer((request, response) => {
  return handler(request, response, {
    public: "./frontend",
  });
});

// start socket server
const io = new Server(server, {});

// when the socket connects
io.on("connection", (socket) => {
  console.log(`connected: ${socket.id}`)

  // emit a event to the client with data
  socket.emit("msg:get", { msg: getMsgs() });

  socket.on("msg:post", data => {
    msg.push({
      user: data.user,
      text: data.text,
      time: Date.now(),
    });

    // 這裡很重要的是，socket 指的是「某一個」與 server 連線的 client
    // 如果是想要廣播讓所有 clients 收到，要用 io，因為 io 表示的是整個 server
    io.emit("msg:get", { msg: getMsgs() });
  })

  socket.on("disconnect", () => {
    console.log(`disconnected: ${socket.id}`)
  })
})

const port = process.env.PORT || 8080;
server.listen(port, () =>
  console.log(`Server running at http://localhost:${port}`)
);
