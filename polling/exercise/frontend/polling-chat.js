const chat = document.getElementById("chat");
const msgs = document.getElementById("msgs");

// let's store all current messages here
let allChat = [];

// the interval to poll at in milliseconds
const INTERVAL = 1000;

// a submit listener on the form in the HTML
chat.addEventListener("submit", function (e) {
  e.preventDefault();
  postNewMsg(chat.elements.user.value, chat.elements.text.value);
  chat.elements.text.value = "";
});

async function postNewMsg(user, text) {
  const data = { user, text };

  const options = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  return fetch("/poll", options);
}

const handleRetry = (BACK_OFF_TIME = 1000) => {
  let numberOfRetries = 0;
  return {
    onSuccess: () => (numberOfRetries = 0),
    onFail: () => numberOfRetries++,
    getElapseTimes: () => BACK_OFF_TIME * numberOfRetries,
  };
};

let retryHandler = handleRetry();
async function getNewMsgs() {
  let json;
  try {
    const res = await fetch("/poll");
    json = await res.json();

    if (res.statusText !== "OK") {
      throw new Error(`request did not succeed: ${res.statusText}`);
    }

    allChat = json.msg;
    render();
    retryHandler.onSuccess();
  } catch (err) {
    console.error(err);
    retryHandler.onFail();
  }
}

function render() {
  // as long as allChat is holding all current messages, this will render them
  // into the ui. yes, it's inefficent. yes, it's fine for this example
  const html = allChat.map(({ user, text, time, id }) =>
    template(user, text, time, id)
  );
  msgs.innerHTML = html.join("\n");
}

// given a user and a msg, it returns an HTML string to render to the UI
const template = (user, msg) =>
  `<li class="collection-item"><span class="badge">${user}</span>${msg}</li>`;

let timeToMakeNextRequest = 0;
async function rafTimer(time) {
  if (timeToMakeNextRequest <= time) {
    await getNewMsgs();
    timeToMakeNextRequest = time + INTERVAL + retryHandler.getElapseTimes();
  }

  requestAnimationFrame(rafTimer);
}

requestAnimationFrame(rafTimer);
