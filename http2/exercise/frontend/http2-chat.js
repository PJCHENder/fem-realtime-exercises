const chat = document.getElementById("chat");
const msgs = document.getElementById("msgs");
const presence = document.getElementById("presence-indicator");

// this will hold all the most recent messages
let allChat = [];

chat.addEventListener("submit", function (e) {
  e.preventDefault();
  postNewMsg(chat.elements.user.value, chat.elements.text.value);
  chat.elements.text.value = "";
});

async function postNewMsg(user, text) {
  const data = {
    user,
    text,
  };

  // request options
  const options = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
      "Content-Type": "application/json",
    },
  };

  // send POST request
  // we're not sending any json back, but we could
  await fetch("/msgs", options);
}

async function getNewMsgs() {
  let reader;

  try {
    // 使用 fetch 一樣可以處理 server 回傳的 stream
    // 但不能直接使用 res.json()，而是要用 res.body.getReader() 的方法來處理 stream
    const res = await fetch("/msgs");

    // opening a readable stream
    // 邏輯上這個 response 並不會結束，可以一直接受後端吐回來的內容
    reader = res.body.getReader();
  } catch (err) {
    console.log("connection error", err);
  }

  presence.innerHTML = '🟢';

  // decode the response
  // if the stream is not done, than keep decoding the response
  const utf8Decoder = new TextDecoder('utf-8');
  do {
    let readerResponse;
    try {
      // read the next chunk of data
      readerResponse = await reader.read();
    } catch (error) {
      console.error("reader failed ", error);
      presence.innerHTML = '🔴';
      return;
    }

    // parse the chunk data
    const chunk = utf8Decoder.decode(readerResponse.value, { stream: true });
    done = readerResponse.done;  // 看 server 是否告知 streaming 已經結束
    if (chunk) {
      try {
        console.log(chunk);
        const json = JSON.parse(chunk);
        allChat = json.msg;
        render();
      } catch (error) {
        console.log("parse the response failed ", error);
      }
    }

  } while (!done);
  presence.innerHTML = '🔴';
}

function render() {
  const html = allChat.map(({ user, text, time, id }) =>
    template(user, text, time, id)
  );
  msgs.innerHTML = html.join("\n");
}

const template = (user, msg) =>
  `<li class="collection-item"><span class="badge">${user}</span>${msg}</li>`;

getNewMsgs();
