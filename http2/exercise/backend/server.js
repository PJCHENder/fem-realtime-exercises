import http2 from "http2";
import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";
import handler from "serve-handler";
import nanobuffer from "nanobuffer";

let connections = [];

const msg = new nanobuffer(50);
const getMsgs = () => Array.from(msg).reverse();

msg.push({
  user: "brian",
  text: "hi",
  time: Date.now(),
});

// the two commands you'll have to run in the root directory of the project are
// (not inside the backend folder)
//
// openssl req -new -newkey rsa:2048 -new -nodes -keyout key.pem -out csr.pem
// openssl x509 -req -days 365 -in csr.pem -signkey key.pem -out server.crt
//
// http2 only works over HTTPS
const __dirname = path.dirname(fileURLToPath(import.meta.url));
const server = http2.createSecureServer({
  cert: fs.readFileSync(path.join(__dirname, "/../server.crt")),
  key: fs.readFileSync(path.join(__dirname, "/../key.pem")),
});

server.on("stream", (stream, headers) => {
  console.log("on stream")

  const path = headers[":path"];
  const method = headers[":method"];

  // streams will open for everything, we want just GETs on /msgs
  if (path === "/msgs" && method === "GET") {

    // 假設一開始有三個 client 連結，但其中一個斷連後，
    // Node 會重複使用這個 id 給新連進來的使用者
    console.log("connected a stream " + stream.id);

    // immediately reply with 200 OK and the coding
    stream.respond({
      ":status": 200,
      // 雖然回傳的是 JSON，但因為是透過 stream 的方式傳送
      // 所以每個封包回去的並不會是完整的一個 JSON
      "content-type": "text/plain; charset=utf-8",
    })

    // write the first response
    stream.write(JSON.stringify({ msg: getMsgs() }));

    // add the stream to the list of connections
    connections.push(stream);

    stream.on("close", () => {
      // remove the stream from the list of connections
      connections = connections.filter(s => s.id !== stream.id);
      console.log("disconnected a stream " + stream.id);
    })
  }
})

server.on("request", async (req, res) => {
  console.log("on request")

  const path = req.headers[":path"];
  const method = req.headers[":method"];

  if (path !== "/msgs") {
    // handle the static assets
    return handler(req, res, {
      public: "./frontend",
    });
  } else if (method === "POST") {
    // get data out of post
    const buffers = [];
    for await (const chunk of req) {
      buffers.push(chunk);
    }
    const data = Buffer.concat(buffers).toString();
    const { user, text } = JSON.parse(data);

    // save the message in the memory of server
    msg.push({
      user,
      text,
      time: Date.now()
    })

    res.end();

    // broadcast the message to all clients
    connections.forEach(stream => {
      stream.write(JSON.stringify({ msg: getMsgs() }));
    })
  }
});

// start listening
const port = process.env.PORT || 8080;
server.listen(port, () =>
  console.log(
    `Server running at https://localhost:${port} - make sure you're on httpS, not http`
  )
);
